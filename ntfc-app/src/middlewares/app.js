const auth = require("basic-auth");
const dotenv = require("dotenv");
// #############
const myAuth = require("./basic-auth");

// #############
const validateAuth = myAuth({ auth, dotenv });
// #############

const { keycloak } = require('../const')


/// keycloak
const session = require('express-session');
const Keycloak = require('keycloak-connect');
const setting = require("./keycloak-config");


const keycloakConfig = {
    clientId: keycloak.clientId,
    // bearerOnly: true,
    serverUrl: keycloak.baseUrl,
    realm: keycloak.realmName,
    realmPublicKey: keycloak.publicKey,
    credentials: {
        secret: keycloak.clientSecret
    }
};

const keycloakAuth = setting({ session, Keycloak, keycloakConfig });


const services = Object.freeze({
    validateAuth,
    keycloakAuth

});

module.exports = services;


module.exports = validateAuth
module.exports = keycloakAuth