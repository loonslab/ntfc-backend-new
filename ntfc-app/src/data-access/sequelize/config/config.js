const { dbsetup } = require("../../../const");

require("dotenv").config();

module.exports = {
  development: {
    username: dbsetup.username,
    password: dbsetup.password,
    host: dbsetup.host,
    port:dbsetup.port,
    database: "ntfc",
    dialect: "postgres",
    dialectOptions: {
      prependSearchPath: true,
    },
    logging: false,
  },
  production: {
    username: "postgres",
    password: "postgresSuperUserPsw",
    host: "ec2-3-132-15-192.us-east-2.compute.amazonaws.com",
    port: 32649,
    database: "ntfc",
    dialect: "postgres",
  },
 
};
