const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");


dotenv.config();
const app = express();

// accessible to any
app.use(cors());

// Body Parser middleware to handle raw JSON files
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


//keycloak
const session = require('express-session')
var memoryStore = new session.MemoryStore();
app.use(session({ secret: 'some secret', resave: false, saveUninitialized: true, store: memoryStore }));

const keycloak = require('./middlewares/app').initKeycloak(memoryStore);
app.use(keycloak.middleware());


const PORT = process.env.PORT || 2000;

const server = app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}.`);
});


//app.use("/api/uploads", require("./routes/file_upload/app"));
//app.use("/api/uploaded_files", require("./routes/uploaded_files/app"));
//app.use("/api/file_upload_items", require("./routes/file_upload_items/app"));

// when invalid routes are entered
app.use(async(req, res) => {
    res.status(404).send(`Route is no where to be found.`);
});

module.exports = app;