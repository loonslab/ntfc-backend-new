const { Parser } = require('json2csv');

const downloadResource = (file_name, fields, data) => {


    const json2csv = new Parser({ fields });
    const csv = json2csv.parse(data);
    const type = "csv"
    return {
        headers: {
            "Content-Type": 'application/csv',
        },
        statusCode: 200,
        body: { csv, file_name, type },
    };
}

module.exports = { downloadResource };