const KcAdminClient = require('@keycloak/keycloak-admin-client').default;


const { keycloak } = require('../const')


const kcAdminClient = new KcAdminClient({
    baseUrl: keycloak.baseUrl,
    realmName: keycloak.realmName
});



const auth = async function() {
    await kcAdminClient.auth({
        username: 'admin',
        password: 'admin',
        grantType: 'password',
        clientId: keycloak.clientId,
        clientSecret: keycloak.clientSecret
    })
};

auth()

setInterval(() => {
    auth()
}, 58 * 1000);

const insert = require("./insert");

const realm = keycloak.realmName

const keycloak_insert = insert({ kcAdminClient, realm });


const services = Object.freeze({
    keycloak_insert
});

module.exports = services;
module.exports = {
    keycloak_insert
};