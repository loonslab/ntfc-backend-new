const add = ({ kcAdminClient, realm }) => {
    return async function post(info) {

        let save = await kcAdminClient.users.create({
            realm: realm,
            username: info.reg_no,
            firstName: info.first_name,
            lastName: info.sur_name,
            email: info.uni_email,
            enabled: true,
            emailVerified: false,
            attributes: {
                type: info.type,
                userId: info.id,
            },
        });

        if (save) {
            const userId = save.id;
            await kcAdminClient.users.resetPassword({
                id: userId,
                credential: {
                    temporary: false,
                    type: 'password',
                    value: info.password,
                },
            });
        }

    };
};

module.exports = add;