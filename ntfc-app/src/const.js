const keycloak = {
 
  clientSecret: process.env.CLIENT_SECRET ||'uH0LDwsP09lXULXFoS9QvnsY1vpFSSat',
  publicKey: process.env.PUBLIC_KEY ||'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArzBiejN+e2phRSotAyzFPhvtcVh+bOBZd7YsNWn+JLMkhVsbwKzxeQBxpTkF1mNjvjoCxDuLaSZTibw5Pyp8e2w+hIKHtYEhURxxyOFpbWcczB1OMs0JqaAxhbydz8CJ9BgMwCwoYfyUNskOelXO5ZCLjAc2bdFKqcfF5GNuAT9m92ROItkOFSEvyHBV4FZehleVxWTC9a78VDseWp+wNXq+Iyq7R4NguGmdJ7OcB1XFQwvx2DsSPrzITL8q6z9Wd+Epas70Vdjg7IYBo20KXRRMpzr8LNe1wns2mW1lEIdf+qqYYL0Qz+WosF0RLSJz+GNNco2TwrfzGRaKz+oqTwIDAQAB',
  publicKey1: 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu15KHp7LuAWP2Vw0F1f+fzntuUwm26YlMY6dUOwo9xs7KPP6f26Z1edZoiTiDUP7ID9WWmFi4bHu3AIvKIKiFC8NhrYNRV8+8WyQQtdnD9OJoM4YxschDTEOUGV7cp7RjBVz0P0nijLUmlzkrMtvRDDfXBLQTe83bUeHAtBuxiWrIefL2rZdJTKjdof5ul8cIiKyLnUHkwvUsP/BAITxKCYwvQWmOp/hTp+OJO583dYiYOT6BgZSvKxjPZ/tB2GqwNl82GAlcWaqHD9WmD3010156XJUwU0SqJqchCbklJiAo/8/Y+osugdmRhdQ9I/xc17+XsjklyUUuwEOwguRqQIDAQAB',
  baseUrl: process.env.KEYCLOAK_BASEURL || 'http://20.55.38.108/auth/',
 realmName: process.env.MY_REALM || 'SwasthaRealm',
  clientId: 'SwasthaClient',
  redirectUri: process.env.KEYCLOAK_REDIRECT_URI || 'http://124.43.176.48:2080/session/signin',
}

const dbsetup = {
  username: process.env.DB_USERNAME || "postgres",
  password: process.env.DB_PASSWORD || "postgres",
  host: process.env.DB_HOST || "127.0.0.1",
  port: process.env.DB_PORT || "5432"
}


const absolute_path = process.env.ABSOLUTE_PATH || "/Loons/VPA/backend/uploads/"


module.exports = {
  keycloak,
  dbsetup,
  absolute_path
};