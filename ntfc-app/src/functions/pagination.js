const paginations = ({}) => {
    return function getPagingData(res, page, limit) {

        const { count: totalItems, rows: data } = res;
        const currentPage = page ? +page : 0;
        const totalPages = Math.ceil(totalItems / limit);

        return { totalItems, data, totalPages, currentPage };
    };
};

module.exports = paginations;