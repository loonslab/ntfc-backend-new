const makeExpressCallback = (controller) => {

    return (req, res) => {

        const grant = req.kauth.grant
        const access_token = grant ? grant.access_token : null
        const content = access_token ? access_token.content : null;
        let type = content ? content.type : null;
        let userId = content ? content.userId : null;
        let body = req.body
        body.userId = userId
        body.userType = type
        const httpRequest = {
            body: body,
            query: req.query,
            params: req.params,
            ip: req.ip,
            method: req.method,
            path: req.path,
            files: req.files ? req.files : req.file,
            headers: {
                "Content-Type": req.get("Content-Type"),
                Referer: req.get("referer"),
                "User-Agent": req.get("User-Agent"),
                Cookie: req.get("Authorization"),
                "Access-Control-Allow-Origin": "*",
            },
        };

      
        controller(httpRequest)
            .then((httpResponse) => {
                if (httpResponse.headers) {
                    res.set("Access-Control-Allow-Origin", "*");
                    res.set(httpResponse.headers);
                }



                if (httpResponse.body.type == "csv") {
                    res.type("csv");
                    res.attachment(httpResponse.body.file_name);
                    res.status(httpResponse.statusCode).send(httpResponse.body.csv);

                } else

                    if (httpResponse.body.type == "view") {
                        res.type(httpResponse.body.extension)
                        res.sendFile(httpResponse.body.path)

                    } else if (httpResponse.body.type == "download") {
                        res.download(httpResponse.body.path, httpResponse.body.fileName, (err) => {
                            if (err) {
                                res.status(500).send({
                                    message: "Could not download the file. " + err,
                                });
                            }
                        });
                    } else {
                        res.type("json");
                        res.status(httpResponse.statusCode).send(httpResponse.body);
                    }

            })
            .catch((e) => res.sendStatus(500));
    };
};

module.exports = makeExpressCallback;